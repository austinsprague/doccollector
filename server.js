const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const fs = require('fs');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
const router = express.Router();


//logging that the server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.post('/login', urlencodedParser, function (req, res) {
    const { email, password } = req.body

    fs.readFile('./login.json', (err, data) => {
        if (err) throw err;
        const json = JSON.parse(data);

        if (email === json.email && password === json.password) {
            res.send({loggedIn:true});
        }
        else {
            res.send({ loggedIn: false });
        }
    });
})


router.post('/login', function (req, res) {
    var user_name = req.body.user;
    var password = req.body.password;


    console.log("User name = " + user_name + ", password is " + password);
    res.end("yes");
});

app.post("/login", function (req, res) {
    let username = "james@gmail.com";
    let password = "12345678";
    console.log(req.body);
    if (
        (req.body.username || req.body.password) &&
        username === req.body.username &&
        password === req.body.password
    ) {
        console.log("Success");
        let token = btoa(username + password);
        return res.send({ token: token });
    }
    console.log("error");
    return res.send(req.body);
});

app.use("/", router);


// create a GET route
app.get('/express_backend', (req, res) => {
    res.send(
        {
            express: 'YOUR EXPRESS BACKEND IS CONNECTED TO REACT'
        });
});

app.get('/api', (req, res) => {
    res.send({
        key: process.env.REACT_APP_PASSWORD});
});

app.get('/json', (req, res) => {
    //To access GET variable use req.query() and req.params() methods.
    fs.readFile('./src/db.json', (err, data) => {
        if (err) throw err;
        const d = JSON.parse(data);
        res.json(d);
    });
});