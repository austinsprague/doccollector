## Doc Collector

### Summary:
* Document upload web application for loan docs with checklist for items needed. 

### Technologies:
* React.js, Express.js, Bootstrap, Filestack API

<img src="https://media.giphy.com/media/aFgLAUgHxPMF4blZoW/giphy.gif" width="600">

<img src="./src/assets/DocCollector_ScreenShot_Login.png" width="500">

<img src="./src/assets/DocCollector_ScreenShot_Checklist.png" width="500">

<img src="./src/assets/DocCollector_ScreenShot_Filestack_upload_1.png" width="500">

<img src="./src/assets/DocCollector_ScreenShot_Filestack_upload_2.png" width="500">

### In the project directory, you can run:
`yarn start`
- Runs the app in the development mode.
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Run server:
 `node --require dotenv/config server.js`
- Note: you will not be able to open the Filestack API from the Upload button without an API key in your .env file. 
- .env --->>> `REACT_APP_PASSWORD='YOUR_API_KEY'`


### Todo:
- [ ] Status bar of loan
- [ ] Notification for todos
- [ ] Message center
- [ ] Loan description
- [ ] Custom upload (remove Filestack API)
- [ ] Wire in Firebase database
- [ ] Refactor login with authentication token
