import React, { Component } from 'react';
import PrimaryNav from "./PrimaryNav";
import SecondaryNav from "./SecondaryNav";
import Checklist from "./Checklist";
import { Row, Col } from "react-bootstrap";



class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        // Call our fetch function below once the component mounts
        if (document.cookie.split(';').some((cookie) => cookie.includes('logged_in=true'))) {
            this.callBackendAPI()
                .then(res => this.setState({ data: res.key }))
                .catch(err => console.log(err));
        }
        else {
            window.location = "/";
        }
    }
    // Fetches our GET route from the Express server. (Note the route we are fetching matches the GET route from server.js
    callBackendAPI = async () => {
        const response = await fetch('/api');
        const body = await response.json();

        if (response.status !== 200) {
            throw Error(body.message)
        }
        return body;
    };

    render() {
        const { loggedIn = true, logoutHandler } = this.props
        if (!this.state.data) {
            return <></>
        }
        else {
            const {first} = this.state.data

            return (
                <div className="container-fluid p-0 m-0">
                
                            <Row className="full-height no-gutters ">
                                <Col xs={3} md={3} className=" bg-light ">
                                    <SecondaryNav loggedIn={loggedIn} profile={" "} />
                                </Col>
                                <Col>
                            <PrimaryNav loggedIn={loggedIn} username={first} logoutHandler={logoutHandler}/>
                                    <Checklist api={this.state.data} />
                        </Col>
                        </Row>
                </div>
            )
        }
    }
}

export default Home
