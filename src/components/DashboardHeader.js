import React, { Component } from 'react';
import { Row, Col } from "react-bootstrap";

class DashboardHeader extends Component {
    constructor(props) {
        super(props)

        this.state = {
            json: {},
            loaded: true
        }
    }


    render() {

        // const houseIcon = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-house-door" viewBox="0 0 16 16">
        //     <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
        // </svg>

        // loader, wait for json to load
        // if (!this.state.loaded) {
        //     return <></>
        // }
        // else {
        const { street, city, state, zip, loan_type, property_type } = this.props.json.properties[0]
        const borrower = this.props.json.borrowers[0]


            return (
                <Row noGutters className="overflow-auto" >
                    <Col>
                        <Row noGutters className="pl-5 pr-5 overflow-auto ">
                            <Col>
                                <p className="small">
                                    Borrower(s):
                                    <br />
                                    <b>{borrower.first} {borrower.middle} {borrower.last}</b>
                                </p>
                            </Col>
                            <Col >
                                <p className="small">
                                    Address:
                                    <br />
                                    <b>{street}
                                    <br />
                                        {city}, {state} {zip}
                                    </b>
                                </p>
                            </Col>
                            <Col>
                                <p className="small">
                                    Property: <b>{property_type}</b>
                                    < br />
                                    Occupancy: <b>{loan_type}</b>
                                </p>
                            </Col>
                            <Col>
                                <p className="small">Status: <em><b><span className="text-white bg-warning">In Progress</span></b></em></p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            )
        // }
    }
}

export default DashboardHeader