import React, { Component } from 'react';
import { Row, Col, Button, Form } from "react-bootstrap";
import bgImage from "../assets/document_files.jpg"

class SignIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            password: "",
            loggedIn: "false"
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleEmailChange = event => {
        console.log("Change", event.target.value)
        this.setState({email: event.target.value})
    };

    handleChange = event => {
        console.log("Change pwd", event.target.value)
        this.setState({[event.target.name]: event.target.value.trim() })
    };

    handleSubmit(event) {

        const email= "ok@gmail.com"
        const password = "ok"
        
        event.preventDefault();
        if (this.state.email === email && this.state.password === password) {
            document.cookie ="logged_in=true"
            this.setState({ loggedIn: true })
            window.location = "/home";
        }
    }


    render() {
        const uploadIcon = <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-cloud-upload" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z" />
            <path fillRule="evenodd" d="M7.646 4.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V14.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3z" />
        </svg>

        return (
            <div noGutters className="full-height " style={{ backgroundImage: ` url(${bgImage})`, backgroundSize: 'cover' }}>
                    <div className="bg-opacity full-height m-0 p-0">
                    <Row className="m-0 p-0 d-flex no-gutters justify-content-center align-items-center">
                
                        <Col className="mt-0 pt-5">
                <div className="login-form mt-5 pt-5">
                    <h1 style={{ fontFamily: ' Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter' }}>{uploadIcon} Doc Collector</h1>
                        <Form onSubmit={this.handleSubmit} className="">
                            <Form.Group controlId="formBasicEmail">
                                {/* <Form.Label>Email address</Form.Label> */}
                                <Form.Control name="email" type="email" placeholder="Email" onChange={this.handleChange} value={this.state.email} />
                                <Form.Text className="text-muted">Hint: "ok@gmail.com"</Form.Text>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                {/* <Form.Label>Password</Form.Label> */}
                                <Form.Control name="password" type="password" placeholder="Password" onChange={this.handleChange} value={this.state.password} />
                                <Form.Text className="text-muted">Hint: "ok"</Form.Text>
                            </Form.Group>
                            <Button type="submit" variant="info" className="block btn-block">Login</Button>
                    </Form>
                        </div>
                </Col>
                </Row>
                    </div>
            </div>
        )
    }
}

export default SignIn