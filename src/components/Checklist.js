import React, { Component } from 'react';
import { Row, Col, Button } from "react-bootstrap";
import json, { properties } from "../db.json";
import * as filestack from 'filestack-js';
import DashboardHeader from "./DashboardHeader";


class Checklist extends Component {
    constructor(props) {
        super(props)

        this.state = {
            json: {},
            loaded: false
        }

        this.handleUploadFile = this.handleUploadFile.bind(this)
    }

      handleUploadFile() {
          const client = filestack.init(this.props.api);
           client.picker().open();
       }


    


    render() {
        const { checklist } = properties[0]
        
        // loader, wait for json to load
        // if (!this.state.loaded) {
        //     return <></>
        // }
        // else {

            // const { checklist } = this.props.json.properties[0]
        
            return (
                            
                <>
                    <DashboardHeader json={json}/>
                    {checklist.map(item => {
                        return (
                            <ChecklistItem {...item} handleUploadFile={this.handleUploadFile} key={item.id} />
                        )
                    })}
                </>
            )
        // }
    }
}

export default Checklist

const ChecklistItem = ({ description, name, needs, handleUploadFile }) => {
    const uploadIcon = <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-cloud-plus-fill" viewBox="0 0 16 16">
        <path d="M8 2a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13h8.906C14.502 13 16 11.57 16 9.773c0-1.636-1.242-2.969-2.834-3.194C12.923 3.999 10.69 2 8 2zm.5 4v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 1 0z" />
    </svg>

    return (
        <Row className="no-gutters m-5 overflow-auto">
            <Col className="list" xs={8}>
                <h5 className="">{name}</h5>
                {description ? <p className="small"><em>{description}</em></p>: null}
                
                {needs.map(need => {
                    return(
                        <div key={need.id} className="ml-5 mr-2">
                            <input className=" form-check-input" type="checkbox" value={need.checked} id={need.id}></input>
                            <label className=" form-check-label" htmlFor={need.id}>{need.description}</label>
                        </div>
                    )
                }
                )
                }
            </Col>
            <Col className="upload">
                <Button className="btn-block btn-info text-white" onClick={handleUploadFile}>{uploadIcon} Upload</Button>
                {/* <p className="small">Successfully uploaded file(s):</p> */}
            </Col>
        </Row>
        )
    }