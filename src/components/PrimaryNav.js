import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown } from "react-bootstrap";


class PrimaryNav extends Component {
    constructor(props) {
        super(props)
        this.handleLogout = this.handleLogout.bind(this)

    }

    handleLogout(){
        document.cookie = "logged_in=false"
    }

    render() {
        const { username = "Austin" } = this.props

        return (
            <>
                <Navbar className="m-3">
                    {/* <Navbar.Toggle aria-controls="basic-navbar-nav" className="mr-5"/> */}
                {/* <Navbar.Collapse id="basic-navbar-nav" className="mr-5"> */}
                        <Nav className="ml-auto mr-5">
                            <NavDropdown title={username} id="basic-nav-dropdown" className="mr-auto">
                            <NavDropdown.Item href="#action/3.1" disabled={true}>Profile</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2" disabled={true}>Settings</NavDropdown.Item>
                            {/* <NavDropdown.Item href="#action/3.3">Settings</NavDropdown.Item> */}
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="/" onClick={this.handleLogout}>Log Out</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                {/* </Navbar.Collapse> */}
            </Navbar>
            </>
        )
    }
}

export default PrimaryNav