import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import './App.css';
import SignIn from "./components/SignIn";
import Home from "./components/Home";
import 'bootstrap/dist/css/bootstrap.min.css';



class App extends Component {

  render() {

      return (
          <BrowserRouter>
            <Switch>
              <Route exact path={"/"} component={SignIn} />
              <Route exact path={"/home"} component={Home} />
            </Switch>
          </BrowserRouter>
      )
  }
}

export default App
